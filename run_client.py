
import os
import webbrowser as wb
from subprocess import Popen, CREATE_NEW_CONSOLE, PIPE, call

os.system('cd simple_chat; echo "Open Browser to localhost:8085"; python -m http.server 8085')

def start_client(host='localhost', port=8085):
    here = os.path.dirname(os.path.realpath(__file__))
    command = """
cd /d {}
echo "Open Browser to {}:{}"
call python -m http.server --cgi {}
pause
    """.format(os.path.join(here, 'simple_chat'), host, port, port)

    file1 = open('run_client.bat','w')
    file1.write(command)
    file1.close()

    p = Popen(os.path.join(here, 'run_client.bat'), creationflags=CREATE_NEW_CONSOLE)

    wb.open('http://{}:{}'.format(host, port))
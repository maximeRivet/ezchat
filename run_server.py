
from sys import executable
import logging
from subprocess import Popen, CREATE_NEW_CONSOLE, PIPE, call
from simple_chat.server import app, socketio
import os
import shlex

logging.info(app.config)


def run(host='localhost', port=5000):
    socketio.run(app, host=host, port=port)

def start_server(host='localhost', port=5000):
    here = os.path.dirname(os.path.realpath(__file__))
    command = """
cd /d {}
SET host={}
SET port={}
call python run_server.py
pause
    """.format(here, host, port)

    file1 = open('run_server.bat','w')
    file1.write(command)
    file1.close()

    p = Popen(os.path.join(here, 'run_server.bat'), creationflags=CREATE_NEW_CONSOLE)

if __name__ == "__main__":

    host = os.environ.get('host', 'localhost')
    port = int(os.environ.get('port', 5000))

    run(host=host, port=port)